#!/usr/bin/python

import smtplib 
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText 
from email.MIMEBase import MIMEBase 
from email import encoders

def load_file(file, file_name):
    read_file = open(file,'rb')
    attach = MIMEBase('multipart', 'encrypted')
    attach.set_payload(read_file.read()) 
    read_file.close()  
    encoders.encode_base64(attach) 
    attach.add_header('Content-Disposition', 'attachment', filename=file_name)
    return attach

def sendemail(addr_to_mail, addr_from_mail):
    smtp_server = 'smtp.gmail.com:587'
    smtp_user   = 'tugmail@gmail.com'
    smtp_pass   = 'tuclave'
    
    email = MIMEMultipart() 
    email['To'] = addr_to_mail
    email['From'] = addr_from_mail
    email['Subject'] = 'Archivo adjunto desde python'
    email.attach(MIMEText('<p style="color:red;" >Envio Archivo adjunto desde python</p>','html'))
    email.attach(load_file('/home/pi/Desktop/hola.txt','chao.txt'))
    smtp = smtplib.SMTP(smtp_server)
    smtp.starttls()
    smtp.login(smtp_user,smtp_pass)
    smtp.sendmail(addr_from_mail, addr_to_mail, email.as_string())
    smtp.quit()
    print "E-mail enviado!"


sendemail('mail_destinatario','tugmail@gmail.com')
